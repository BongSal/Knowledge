package com.knowledge.rupp.kosal.knowledge.activities.detail_feeds;

import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.adapters.CommentsAdapter;
import com.knowledge.rupp.kosal.knowledge.fragments.main.FeedsFragment;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Comments;
import com.knowledge.rupp.kosal.knowledge.utils.Constant;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements ValueEventListener,
        View.OnClickListener, CommentsAdapter.OnItemClickListener{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.author) TextView author;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.posted_by) TextView postedBy;
    @BindView(R.id.comment) ImageView comment;
    @BindView(R.id.like) ImageView like;
    @BindView(R.id.content) TextView content;
    @BindView(R.id.progress_wheel) ProgressWheel progressWheel;
    @BindView(R.id.nested_scroll_view) NestedScrollView nestedScrollView;
    @BindView(R.id.count_like) TextView textViewCountLike;

    private final String COMMENTS = "comments";
    private final String FEEDS = "feeds";
    private final String IMAGE = "image";

    private final String TAG = DetailActivity.class.getSimpleName();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference reference;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference;
    private List<Comments> commentsList = new ArrayList<>();
    private CommentsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.information));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentsAdapter(this, commentsList, this);
        recyclerView.setAdapter(adapter);

        reference = database.getReference().child(this.FEEDS + "/" + getIntent().getStringExtra(FeedsFragment.KEY));
        like.setOnClickListener(this);
        comment.setOnClickListener(this);
        reference.addListenerForSingleValueEvent(DetailActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: finish(); break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Firebase on Data change
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        String mainTitle = (String) dataSnapshot.child(Constant.TITLE).getValue();
        String mainAuthor = (String) dataSnapshot.child(Constant.AUTHOR).getValue();
        String mainPosted_by = (String) dataSnapshot.child(Constant.POSTED_BY).getValue();
        String mainContent = (String) dataSnapshot.child(Constant.CONTENT).getValue();
        long mainLike = (long) dataSnapshot.child(Constant.LIKE).getValue();

        try {
            storageReference = storage.getReference().child(DetailActivity.this.FEEDS + "/" + dataSnapshot.child(DetailActivity.this.IMAGE).getValue());
            Glide.with(DetailActivity.this).using(new FirebaseImageLoader()).load(storageReference).into(imageView);
            title.setText(mainTitle);
            author.setText(mainAuthor);
            postedBy.setText(mainPosted_by);
            content.setText(mainContent);
            textViewCountLike.setText(String.valueOf(mainLike));
        }
        catch (Exception e){
            Utils.log(TAG, e.getMessage());
        }

        for (DataSnapshot comment : dataSnapshot.child(DetailActivity.this.COMMENTS).getChildren()) {
            String key = comment.getKey(), content = (String) comment.child("content").getValue();
            String created_at = (String) comment.child("created_at").getValue(), image = (String) comment.child("image").getValue();
            String name = (String) comment.child("name").getValue();
            commentsList.add(new Comments(key, content, created_at, image, name));
        }
        nestedScrollView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    // Firebase on cancel
    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.like:
                Utils.log(TAG, "click like");
                break;
            case R.id.comment:
                Intent intent = new Intent(this, CommentActivity.class);
                intent.putExtra(FeedsFragment.KEY, getIntent().getStringExtra(FeedsFragment.KEY));
                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up); // animation
                break;
            default: break;
        }
    }

    // Like Click Listener
    @Override
    public void onLikeClickListener(int position) {
        Utils.log(TAG, "Like click position " + position);
    }

    // Comment Click Listener
    @Override
    public void onReplyClickListener(int position) {
        Utils.log(TAG, "Reply click position " + position);
    }
}