package com.knowledge.rupp.kosal.knowledge.model.dataset;

import com.google.gson.annotations.SerializedName;

public class MostViews {
    private String key;
    private String title;
    private String content;
    private String image;
    private String author;
    private String postedBy;
    private long like;
    private long rate;
    private String createdAt;
    private String updatedAt;

    public MostViews(String key, String title, String content, String image, String author, String postedBy, long like, long rate, String createdAt, String updatedAt) {
        this.key = key;
        this.title = title;
        this.content = content;
        this.image = image;
        this.author = author;
        this.postedBy = postedBy;
        this.like = like;
        this.rate = rate;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getLike() {
        return String.valueOf(like);
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setLike(long like) {
        this.like = like;
    }

    public long getRate() {
        return rate;
    }

    public void setRate(long rate) {
        this.rate = rate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
