package com.knowledge.rupp.kosal.knowledge.interfaces;


import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface NetworkResult {
    void notifySuccess(JSONObject response);
    void notifyError(VolleyError error);
}
