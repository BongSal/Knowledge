package com.knowledge.rupp.kosal.knowledge.networks;


public class URL {
    private static final String serverUrl = "http://192.168.222.1/Knowledge/public/";
    private static final String imagePath = serverUrl + "images/";

    // Feeds
    public static final String GET_FEEDS = serverUrl + "feeds/get-feeds";
    public static final String FEED_IMAGE_PATH = imagePath + "feeds/";
    // Most views
    public static final String GET_MOST_VIEWS = serverUrl + "most-views/get-most-views";
    public static final String MOST_VIEWS_IMAGE_PATH = imagePath + "most-views/";
    // Questions
    public static final String GET_QUESTIONS = serverUrl + "questions/get-questions";
    public static final String QUESTION_IMAGE_PATH = imagePath + "questions/";
}
