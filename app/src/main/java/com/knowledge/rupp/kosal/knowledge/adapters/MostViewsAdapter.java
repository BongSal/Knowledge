package com.knowledge.rupp.kosal.knowledge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.dataset.MostViews;
import com.knowledge.rupp.kosal.knowledge.networks.URL;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MostViewsAdapter extends RecyclerView.Adapter<MostViewsAdapter.MyViewHolder>{

    private List<MostViews> dataList;
    private Context context;

    public MostViewsAdapter(Context context, List<MostViews> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public MostViewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_most_views, parent, false);
        return new MostViewsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MostViewsAdapter.MyViewHolder holder, int position) {
        holder.titleFeeds.setText(dataList.get(position).getTitle());
        holder.authorFeeds.setText(dataList.get(position).getAuthor());
        holder.likeFeeds.setText(dataList.get(position).getLike());
        holder.postedDateFeeds.setText(dataList.get(position).getCreatedAt());
//        Glide.with(context).load(URL.MOST_VIEWS_IMAGE_PATH + dataList.get(position).getImage()).into(holder.imageFeeds);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_most_views) TextView titleFeeds;
        @BindView(R.id.author_most_views) TextView authorFeeds;
        @BindView(R.id.posted_date_most_veiws) TextView postedDateFeeds;
        @BindView(R.id.like_most_views) TextView likeFeeds;
        @BindView(R.id.image_most_views) ImageView imageFeeds;
        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
