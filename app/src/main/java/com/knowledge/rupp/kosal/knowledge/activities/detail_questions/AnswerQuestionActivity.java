package com.knowledge.rupp.kosal.knowledge.activities.detail_questions;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.databases.LoginSessionManager;
import com.knowledge.rupp.kosal.knowledge.model.dataset.AnswerQuestion;
import com.knowledge.rupp.kosal.knowledge.utils.Constant;
import com.knowledge.rupp.kosal.knowledge.utils.Image;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswerQuestionActivity extends AppCompatActivity implements OnCompleteListener,
        View.OnClickListener
{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.edit_text_answer) EditText editTextAnswer;
    @BindView(R.id.button_add_image) Button buttonAddImage;
    @BindView(R.id.image_view) ImageView imageView;

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private LoginSessionManager sessionManager;
    private Uri filePath = null;
    private Menu menu;

    private final int RESULT_LOAD_IMAGE = 3;
    private final String TAG = AnswerQuestionActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.information));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage(getString(R.string.please_wait) + "...");
        progressDialog.setTitle(getString(R.string.uploading));
        buttonAddImage.setOnClickListener(this);
        sessionManager = new LoginSessionManager(this);
        databaseReference = firebaseDatabase.getReference(Constant.ANSWERS_QUESTION + "/" + getIntent().getStringExtra(Constant.KEY));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post, menu);
        this.menu = menu;
        return true;
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_menu_upload:
                if (isSend()) {
                    progressDialog.show();
                    String answer = editTextAnswer.getText().toString();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Constant.CONTENT, answer);
                    map.put(Constant.CREATED_AT, new SimpleDateFormat("mm dd, yyyy").format(Calendar.getInstance().getTime()));
                    map.put(Constant.LIKE, 0);
                    map.put(Constant.USER, sessionManager.getInformation(LoginSessionManager.ID));
                    databaseReference.push().setValue(map).addOnCompleteListener(this);

                    if (filePath != null) {
                        StorageReference riversRef = FirebaseStorage.getInstance().getReference("questions_answer/" + filePath.getLastPathSegment());
                        riversRef.putFile(filePath)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Utils.log(TAG, "success upload");
                                progressDialog.hide();
                                AnswerQuestionActivity.this.finish();
                            }
                        });
                    }
                }
                else {
                    Toast.makeText(this, getString(R.string.please_input_your_answer), Toast.LENGTH_SHORT).show();
                }
                break;
            case android.R.id.home: finish(); break;
            default: break;
        }
        return true;
    }

    @Override
    public void onComplete(@NonNull Task task) {
        if (task.isComplete()) {
            progressDialog.hide();
            Toast.makeText(this, getString(R.string.successfully_uploaded), Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_image:
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;
            default: break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
            this.filePath = data.getData();
            Glide.with(this).load(filePath).into(imageView);
            menu.findItem(R.id.item_menu_upload).setIcon(R.drawable.ic_sendable_white);
        }
        else {
            menu.findItem(R.id.item_menu_upload).setIcon(R.drawable.ic_send);
        }
    }

    private boolean isSend() {
        return !editTextAnswer.getText().toString().isEmpty();
    }

}
