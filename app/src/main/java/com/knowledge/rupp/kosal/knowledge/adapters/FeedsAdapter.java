package com.knowledge.rupp.kosal.knowledge.adapters;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Feeds;
import com.knowledge.rupp.kosal.knowledge.networks.URL;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViewHolder>{

    private List<Feeds> dataList;
    private final String TAG = FeedsAdapter.class.getSimpleName();
    private Context context;
    private FirebaseStorage storage =  FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference("feeds");

    public FeedsAdapter(Context context, List<Feeds> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public FeedsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feeds, parent, false);
        return new FeedsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FeedsAdapter.MyViewHolder holder, int position) {
        holder.titleFeeds.setText(dataList.get(position).getTitle());
        holder.authorFeeds.setText(dataList.get(position).getAuthor());
        holder.likeFeeds.setText(dataList.get(position).getLike());
        holder.postedDateFeeds.setText(dataList.get(position).getCreatedAt());
        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(storageReference.child(dataList.get(position).getImage()))
                .into(holder.imageFeeds);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_feeds) TextView titleFeeds;
        @BindView(R.id.author_feeds) TextView authorFeeds;
        @BindView(R.id.posted_date_feeds) TextView postedDateFeeds;
        @BindView(R.id.like_feeds) TextView likeFeeds;
        @BindView(R.id.image_feeds) ImageView imageFeeds;
        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
