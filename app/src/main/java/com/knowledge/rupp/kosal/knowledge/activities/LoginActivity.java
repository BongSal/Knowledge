package com.knowledge.rupp.kosal.knowledge.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.databases.LoginSessionManager;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements FacebookCallback<LoginResult>, View.OnClickListener {

    @BindView(R.id.facebook_login_button) LoginButton facebookLoginButton;
    @BindView(R.id.login_button) Button loginButton;
    @BindView(R.id.text_input_username) EditText editTextUsername;
    @BindView(R.id.text_input_password) EditText editTextPassword;
    @BindView(R.id.text_input_layout_username) TextInputLayout textInputLayoutUsername;
    @BindView(R.id.text_input_layout_password) TextInputLayout textInputLayoutPassword;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private CallbackManager callbackManager;
    private FirebaseAuth firebaseAuth;
    private LoginSessionManager sessionManager;
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference("users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sessionManager = new LoginSessionManager(this);
        firebaseAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        facebookLoginButton.setReadPermissions("email", "public_profile");
        facebookLoginButton.registerCallback(callbackManager, this);
        loginButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.log(TAG, "on start is created");
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            Utils.log(TAG, "User is not logged in");
        }
    }

    // Login facebook success
    @Override
    public void onSuccess(LoginResult loginResult) {
        handleFacebookAccessToken(loginResult.getAccessToken());
    }

    // Cancel facebook login
    @Override
    public void onCancel() {
        Utils.log(TAG, "Something went wrong");
    }

    // Facebook error Exception
    @Override
    public void onError(FacebookException error) {
        Utils.log(TAG, error.getMessage());
    }


    // On Result Facebook
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Handle access token and Register to firebase
    private void handleFacebookAccessToken(final AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            if (user != null) {
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d(TAG, "signInWithCredential:failure", task.getException());
                            FirebaseAuth.getInstance().signOut();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button: // Login button
                Utils.log(TAG, "Click Login");
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();
                if (username.isEmpty()) {
                    Utils.log(TAG, "username is empty");
                    textInputLayoutUsername.setError(getString(R.string.user_name_is_required));
                }
                else {
                    final ProgressDialog dialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
                    dialog.setMessage(getString(R.string.logging_in));
                    dialog.show();
                    Task<AuthResult> auth = firebaseAuth.signInWithEmailAndPassword(username, password);
                    auth.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                String id = firebaseAuth.getUid();
                                Log.d(TAG, "User id is : " + id);
                                reference.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        sessionManager.setUserInformation(dataSnapshot.getKey(), (String) dataSnapshot.child("name").getValue(), null,
                                                (String) dataSnapshot.child("email").getValue(), (String) dataSnapshot.child("image").getValue(),
                                                (String) dataSnapshot.child("phone").getValue(), null);
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));

//                                        Utils.log(TAG, "On data change session :" +  sessionManager.getInformation(LoginSessionManager.PROFILE));

//                                        firebaseAuth.signOut();
                                        dialog.dismiss();
                                        finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(LoginActivity.this, getString(R.string.wrong_email_or_password), Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }
                                });
                                Utils.log(TAG, "success login");
                            }
                            else {
                                Utils.log(TAG, "fail login");
                            }

                        }
                    }).addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Utils.log(TAG, "On Failure Listener" + e.getMessage());
                            dialog.dismiss();
                        }
                    });
                }
                break;
        }
    }
}
