package com.knowledge.rupp.kosal.knowledge.activities.detail_feeds;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.adapters.CommentsAdapter;
import com.knowledge.rupp.kosal.knowledge.fragments.main.FeedsFragment;
import com.knowledge.rupp.kosal.knowledge.model.databases.LoginSessionManager;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Comments;
import com.knowledge.rupp.kosal.knowledge.utils.Constant;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends AppCompatActivity implements ChildEventListener, ValueEventListener,
        CommentsAdapter.OnItemClickListener, TextWatcher, View.OnClickListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.edit_text_comment) EditText editTextComment;
    @BindView(R.id.image_button_send) ImageButton imageButtonSend;
    @BindView(R.id.constraint_comment_edit_text) ConstraintLayout constraintLayoutComment;
    @BindView(R.id.linear_parent_progress_wheel) LinearLayout parentProgressWheel;

    private LoginSessionManager sessionManager;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private List<Comments> commentsList = new ArrayList<>();
    private CommentsAdapter adapter;
    private DatabaseReference reference = database.getReference();

    private final String TAG = CommentActivity.class.getSimpleName();
    private final String FEEDS = "feeds";
    private boolean isSetColor = true, isSetNoColor = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.comments));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sessionManager = new LoginSessionManager(this);

        editTextComment.addTextChangedListener(this);
        imageButtonSend.setOnClickListener(this);

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentsAdapter(this, commentsList, this);
        recyclerView.setAdapter(adapter);
        reference.child(this.FEEDS + "/" + getIntent().getStringExtra(FeedsFragment.KEY) + "/comments").orderByKey()
                .addChildEventListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home: finish(); break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        parentProgressWheel.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        constraintLayoutComment.setVisibility(View.VISIBLE);
        String key = dataSnapshot.getKey(),
                content = (String) dataSnapshot.child("content").getValue(),
                created_at = (String) dataSnapshot.child("created_at").getValue(),
                image = (String) dataSnapshot.child("image").getValue(),
                name = (String) dataSnapshot.child("name").getValue();
        commentsList.add(new Comments(key, content, created_at, image, name));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
//        for (DataSnapshot comment : dataSnapshot.getChildren()) {
//            String key = comment.getKey(), content = (String) comment.child("content").getValue();
//            String created_at = (String) comment.child("created_at").getValue(), image = (String) comment.child("image").getValue();
//            String name = (String) comment.child("name").getValue();
//            commentsList.add(new Comments(key, content, created_at, image, name));
//        }
//        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    public void onLikeClickListener(int position) {

    }

    @Override
    public void onReplyClickListener(int position) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (count != 0) {
            if (isSetColor) {
                imageButtonSend.setImageResource(R.drawable.ic_sendable);
                imageButtonSend.setClickable(true);
                isSetColor = false;
                isSetNoColor = true;
            }
        }
        else {
            if (isSetNoColor) {
                imageButtonSend.setImageResource(R.drawable.ic_send);
                imageButtonSend.setClickable(false);
                isSetColor = true;
                isSetNoColor = false;
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        Utils.log(TAG, "After Text change");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_button_send:
                Map<String, String> data = new HashMap<>();
                data.put("content", editTextComment.getText().toString());
                data.put("created_at", new Date().toString());
                data.put("image", sessionManager.getInformation(LoginSessionManager.PROFILE));
                data.put("name", sessionManager.getInformation(LoginSessionManager.NAME));
                reference.child(this.FEEDS + "/" + getIntent().getStringExtra(FeedsFragment.KEY) + "/comments").push().setValue(data);
                editTextComment.setText("");
                imageButtonSend.setClickable(false);
                imageButtonSend.setImageResource(R.drawable.ic_send);
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                break;
            default: break;
        }
    }
}
