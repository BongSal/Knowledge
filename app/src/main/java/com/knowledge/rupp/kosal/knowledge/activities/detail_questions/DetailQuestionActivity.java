package com.knowledge.rupp.kosal.knowledge.activities.detail_questions;

import android.content.Intent;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.adapters.AnswersAdapter;
import com.knowledge.rupp.kosal.knowledge.fragments.main.QuestionFragment;
import com.knowledge.rupp.kosal.knowledge.model.dataset.AnswerQuestion;
import com.knowledge.rupp.kosal.knowledge.utils.Constant;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailQuestionActivity extends AppCompatActivity
        implements ValueEventListener, View.OnClickListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.content) TextView content;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.nested_scroll_view) NestedScrollView nestedScrollView;
    @BindView(R.id.progress_wheel) ProgressWheel progressWheel;
    @BindView(R.id.answer) ImageView answer;

    private final String QUESTIONS = "questions", CONTENT = "content";
    private final String IMAGE = "image";
    private final String TITLE = "title";

    private static final String TAG = DetailQuestionActivity.class.getSimpleName();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference reference;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference;
    private List<AnswerQuestion> answersList = new ArrayList<>();
    private AnswersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_question);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.information));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AnswersAdapter(this, answersList);
        recyclerView.setAdapter(adapter);

        answer.setOnClickListener(this);

        reference = database.getReference()
                .child(this.QUESTIONS + "/" + getIntent().getStringExtra(QuestionFragment.KEY));

        database.getReference("answers_question/" + getIntent().getStringExtra(QuestionFragment.KEY))
                .addListenerForSingleValueEvent(this);

        reference.addListenerForSingleValueEvent(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: finish(); break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Firebase on Data change
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        // title
        String mainTitle = (String) dataSnapshot.child(DetailQuestionActivity.this.TITLE).getValue();
        // content
        String mainContent = (String) dataSnapshot.child(DetailQuestionActivity.this.CONTENT).getValue();
        // image
        storageReference = storage.getReference().child(DetailQuestionActivity.this.QUESTIONS + "/" + dataSnapshot
                .child(DetailQuestionActivity.this.IMAGE).getValue());

        try {
            // set content, title and image
            content.setText(mainContent);
            title.setText(mainTitle);
            Glide.with(DetailQuestionActivity.this)
                    .using(new FirebaseImageLoader())
                    .load(storageReference).into(imageView);
        }
        catch (Exception e) {
            Utils.log(TAG, e.getMessage());
        }

        for (DataSnapshot answer : dataSnapshot.getChildren()) {
            if (!answer.hasChildren()) break;
            String key = answer.getKey(),
                    content = (String) answer.child("content").getValue(),
                    created_at = (String) answer.child("created_at").getValue(),
                    image = (String) answer.child("image").getValue(),
                    name = (String) answer.child("user").getValue(),
                    userId = (String) answer.child("user").getValue();
            long like = (long) answer.child("like").getValue();
            answersList.add(new AnswerQuestion(userId, key, content, created_at, image, name, like));
        }
        nestedScrollView.setVisibility(View.VISIBLE);
        progressWheel.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        // firebase
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.like:
                Utils.log(TAG, "click like");
                break;
            case R.id.answer:
                Intent intent = new Intent(this, AnswerQuestionActivity.class);
                intent.putExtra(Constant.KEY, getIntent().getStringExtra(Constant.KEY));
                startActivity(intent);
                break;
            default: break;
        }
    }
}
