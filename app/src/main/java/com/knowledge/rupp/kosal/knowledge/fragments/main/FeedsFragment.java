package com.knowledge.rupp.kosal.knowledge.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.activities.PostFeedActivity;
import com.knowledge.rupp.kosal.knowledge.activities.detail_feeds.DetailActivity;
import com.knowledge.rupp.kosal.knowledge.adapters.FeedsAdapter;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Feeds;
import com.knowledge.rupp.kosal.knowledge.utils.RecyclerItemClickListener;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedsFragment extends Fragment implements
        RecyclerItemClickListener.OnItemClickListener, ValueEventListener, View.OnClickListener {

    @BindView(R.id.feeds_recycler_view) RecyclerView feedsRecyclerView;
    @BindView(R.id.linear_parent_progress_wheel) LinearLayout progress_wheel_parent;
    @BindView(R.id.fab) FloatingActionButton fab;

    public static final String KEY = "key";
    public static boolean isFeedLoaded = true;
    private static final String TAG = FeedsFragment.class.getSimpleName();
    private FeedsAdapter adapter;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private List<Feeds> feedsList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feeds, container, false);
        ButterKnife.bind(this, rootView);
        fab.setOnClickListener(this);
        database = FirebaseDatabase.getInstance(); // database reference
        reference = database.getReference("feeds");
        reference.orderByChild("created_at").addListenerForSingleValueEvent(this);
        feedsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        feedsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), feedsRecyclerView, this));
        adapter = new FeedsAdapter(getActivity(), feedsList);
        feedsRecyclerView.setAdapter(adapter);
        isFeedLoaded = false;
        return rootView;
    }

    // Recycler view click
    @Override
    public void onItemClick(View view, int position) {
        Intent  intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(KEY, feedsList.get(position).getKey());
        startActivity(intent);
    }

    // Recycler view long click
    @Override
    public void onLongItemClick(View view, int position) {
        Utils.log(TAG, "on long click");
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot data : dataSnapshot.getChildren()) {
            String author = (String) data.child("author").getValue();
            String content = (String) data.child("content").getValue();
            String created_at = (String) data.child("created_at").getValue();
            String updated_at = (String) data.child("updated_at").getValue();
            String title = (String) data.child("title").getValue();
            String postedBy = (String) data.child("posted_by").getValue();
            String image = (String) data.child("image").getValue();
            long like = (long) data.child("like").getValue();
            long rate = (long) data.child("rate").getValue();
            feedsList.add(new Feeds(data.getKey(), title, content, image, author, postedBy, like, rate, created_at, updated_at));
        }
        progress_wheel_parent.setVisibility(View.GONE);
        feedsRecyclerView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Utils.log(TAG, databaseError.getMessage());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                startActivity(new Intent(getActivity(), PostFeedActivity.class));
                break;
            default: break;
        }
    }
}