package com.knowledge.rupp.kosal.knowledge.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.activities.PostQuestionActivity;
import com.knowledge.rupp.kosal.knowledge.activities.detail_questions.DetailQuestionActivity;
import com.knowledge.rupp.kosal.knowledge.adapters.QuestionsAdapter;
import com.knowledge.rupp.kosal.knowledge.interfaces.NetworkResult;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Questions;
import com.knowledge.rupp.kosal.knowledge.networks.Network;
import com.knowledge.rupp.kosal.knowledge.networks.URL;
import com.knowledge.rupp.kosal.knowledge.utils.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener,
        ValueEventListener, View.OnClickListener {

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.linear_parent_progress_wheel) LinearLayout progress_wheel_parent;
    @BindView(R.id.fab) FloatingActionButton fab;

    public static final String KEY = "key";
    private List<Questions> dataList = new ArrayList<>();
    private final String QUESTIONS = "questions";
    private QuestionsAdapter adapter;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference reference = database.getReference(this.QUESTIONS);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, rootView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new QuestionsAdapter(getActivity(), dataList);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, this));
        reference.addListenerForSingleValueEvent(this);
        fab.setOnClickListener(this);
        return rootView;
    }
    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), DetailQuestionActivity.class);
        intent.putExtra(KEY, dataList.get(position).getKey());
        startActivity(intent);
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot data : dataSnapshot.getChildren()) {
            Questions questions = new Questions(data.getKey(), (String) data.child("title").getValue(),
                    (String) data.child("content").getValue(), (String) data.child("image").getValue(),
                    (String) data.child("posted_by").getValue(), (long) data.child("rate").getValue(),
                    (String) data.child("created_at").getValue(), (String) data.child("updated_at").getValue());
            dataList.add(questions);
        }
        progress_wheel_parent.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        // do something
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                startActivity(new Intent(getActivity(), PostQuestionActivity.class));
                break;
            default: break;
        }
    }
}
