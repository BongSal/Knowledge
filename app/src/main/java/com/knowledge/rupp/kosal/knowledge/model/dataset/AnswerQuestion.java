package com.knowledge.rupp.kosal.knowledge.model.dataset;

public class AnswerQuestion {
    private String userId, key, content, created_at, image, name;
    private long like;

    public AnswerQuestion(String userId, String key, String content, String created_at, String image, String name, long like) {
        this.userId = userId;
        this.key = key;
        this.content = content;
        this.created_at = created_at;
        this.image = image;
        this.name = name;
        this.like = like;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLike() {
        return like + "";
    }

    public void setLike(long like) {
        this.like = like;
    }
}
