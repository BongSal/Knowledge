package com.knowledge.rupp.kosal.knowledge.services;

import com.google.firebase.messaging.RemoteMessage;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private final String TAG = FirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Utils.log(TAG, "on message received from : " + remoteMessage.getFrom());

    }
}
