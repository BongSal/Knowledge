package com.knowledge.rupp.kosal.knowledge.fragments.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.adapters.MostViewsAdapter;
import com.knowledge.rupp.kosal.knowledge.interfaces.NetworkResult;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Feeds;
import com.knowledge.rupp.kosal.knowledge.model.dataset.MostViews;
import com.knowledge.rupp.kosal.knowledge.networks.Network;
import com.knowledge.rupp.kosal.knowledge.networks.URL;
import com.knowledge.rupp.kosal.knowledge.utils.RecyclerItemClickListener;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MostViewFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener {

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    private List<MostViews> dataList = new ArrayList<>();
    private static final String TAG = MostViewFragment.class.getSimpleName();
    private MostViewsAdapter adapter;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference reference = database.getReference().child("feeds");

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_most_view, container, false);
        ButterKnife.bind(this, rootView);
        reference.orderByChild("view").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    String author = (String) data.child("author").getValue();
                    String content = (String) data.child("content").getValue();
                    String created_at = (String) data.child("created_at").getValue();
                    String updated_at = (String) data.child("updated_at").getValue();
                    String title = (String) data.child("title").getValue();
                    String postedBy = (String) data.child("posted_by").getValue();
                    String image = (String) data.child("image").getValue();
                    long like = (long) data.child("like").getValue();
                    long rate = (long) data.child("rate").getValue();
                    dataList.add(new MostViews(data.getKey(), title, content, image, author, postedBy, like, rate, created_at, updated_at));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MostViewsAdapter(getActivity(), dataList);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, this));
        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onLongItemClick(View view, int position) {

    }
}