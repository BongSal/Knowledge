package com.knowledge.rupp.kosal.knowledge.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.databases.LoginSessionManager;
import com.knowledge.rupp.kosal.knowledge.utils.Constant;
import com.knowledge.rupp.kosal.knowledge.utils.Image;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostFeedActivity extends AppCompatActivity implements View.OnClickListener, OnCompleteListener{

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.button_add_image) Button buttonAddImage;
    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.title) EditText editTextTitle;
    @BindView(R.id.content) EditText editContent;
    @BindView(R.id.author) EditText editTextAuthor;

    private final String TAG = PostFeedActivity.class.getSimpleName();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference("feeds");
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();
    private final int RESULT_LOAD_IMAGE = 1;
    private Uri filePath;
    private boolean isSend = false;
    private Menu menu;
    private LoginSessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_feed);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.post_article));
        sessionManager = new LoginSessionManager(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        buttonAddImage.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: finish(); break;
            case R.id.item_menu_upload:
                if (isSend) {
                    StorageReference riversRef = storageReference.child("feeds/" + filePath.getLastPathSegment());
                    riversRef.putFile(filePath)
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            HashMap<String, Object> map = new HashMap<>();
                            map.put(Constant.TITLE, editTextTitle.getText().toString());
                            map.put(Constant.AUTHOR, editTextAuthor.getText().toString());
                            map.put(Constant.CONTENT, editContent.getText().toString());
                            map.put(Constant.POSTED_BY, sessionManager.getInformation(LoginSessionManager.ID));
                            map.put(Constant.UPDATED_AT, "Jan 18, 2018");
                            map.put(Constant.CREATED_AT, "Jan 18, 2018");
                            map.put(Constant.IMAGE, filePath.getLastPathSegment());
                            map.put(Constant.VIEW, 0);
                            map.put(Constant.RATE, 0);
                            map.put(Constant.LIKE, 0);
                            databaseReference.push().setValue(map).addOnCompleteListener(PostFeedActivity.this);
                        }
                    });
                    Toast.makeText(this, getString(R.string.image_is_being_uploaded), Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(this, getString(R.string.please_choose_image), Toast.LENGTH_SHORT).show();
                break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_image:
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;
            default: break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
            this.filePath = data.getData();
            Glide.with(this).load(filePath).into(imageView);
            menu.findItem(R.id.item_menu_upload).setIcon(R.drawable.ic_sendable_white);
            isSend = true;
        }
        else {
            isSend = false;
            menu.findItem(R.id.item_menu_upload).setIcon(R.drawable.ic_send);
        }
    }

    @Override
    public void onComplete(@NonNull Task task) {
        Utils.log(TAG, "Success upload article");
    }
}
