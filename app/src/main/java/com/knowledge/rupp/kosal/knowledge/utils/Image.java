package com.knowledge.rupp.kosal.knowledge.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

public class Image {

    public static String resizeBase64Image(Bitmap image, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        image = Bitmap.createScaledBitmap(image, width, height, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }
}
