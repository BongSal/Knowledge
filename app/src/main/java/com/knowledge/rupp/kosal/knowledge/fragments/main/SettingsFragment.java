package com.knowledge.rupp.kosal.knowledge.fragments.main;


import android.content.Intent;
import android.media.MediaCas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.activities.LoginActivity;
import com.knowledge.rupp.kosal.knowledge.activities.MainActivity;
import com.knowledge.rupp.kosal.knowledge.model.databases.LoginSessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends Fragment implements View.OnClickListener{

    @BindView(R.id.logout_button) Button logOutButton;
    private FirebaseAuth firebaseAuth;
    private LoginSessionManager sessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        sessionManager = new LoginSessionManager(getActivity());
        firebaseAuth = FirebaseAuth.getInstance();
        logOutButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout_button:
                firebaseAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                sessionManager.logoutUser();
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }
}
