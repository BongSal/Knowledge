package com.knowledge.rupp.kosal.knowledge.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {

    private final String TAG = FirebaseInstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Utils.log(TAG, "on token refresh" + FirebaseInstanceId.getInstance().getToken());
    }
}