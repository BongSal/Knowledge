package com.knowledge.rupp.kosal.knowledge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Questions;
import com.knowledge.rupp.kosal.knowledge.networks.URL;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.MyViewHolder>{

    private List<Questions> dataList;
    private Context context;
    private final String QUESTIONS = "questions";
    private StorageReference reference = FirebaseStorage.getInstance().getReference(QUESTIONS);

    public QuestionsAdapter(Context context, List<Questions> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public QuestionsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_questions, parent, false);
        return new QuestionsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionsAdapter.MyViewHolder holder, int position) {
        holder.titleQuestions.setText(dataList.get(position).getTitle());
        holder.postedTimeQuestions.setText(dataList.get(position).getCreatedAt()); // TODO: fix later
        Glide.with(context).using(new FirebaseImageLoader()).load(reference.child(dataList.get(position).getImage())).into(holder.imageQuestions);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_questions) TextView titleQuestions;
        @BindView(R.id.posted_time_questions) TextView postedTimeQuestions;
        @BindView(R.id.image_questions) ImageView imageQuestions;
        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
