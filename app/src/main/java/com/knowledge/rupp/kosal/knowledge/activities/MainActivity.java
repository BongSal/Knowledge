package com.knowledge.rupp.kosal.knowledge.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.fragments.main.FeedsFragment;
import com.knowledge.rupp.kosal.knowledge.fragments.main.NotificationFragment;
import com.knowledge.rupp.kosal.knowledge.fragments.main.QuestionFragment;
import com.knowledge.rupp.kosal.knowledge.fragments.main.SettingsFragment;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.navigation) BottomNavigationView navigationBottom;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Utils.log(TAG, "On MainActivity Created");
        if (getIntent().getExtras() != null) {
            Utils.log(TAG, "intent is not null");
            Toast.makeText(this, getIntent().getExtras().getString("key") + "1234", Toast.LENGTH_SHORT).show();
        }
        else {
            Utils.log(TAG, "message key is null");
        }
        Utils.log(TAG, "My Token is : " + FirebaseInstanceId.getInstance().getToken());
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_main, new FeedsFragment()).commit();
        navigationBottom.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()) {
            case R.id.navigation_home:
                transaction.replace(R.id.frame_layout_main, new FeedsFragment()).commit();
                return true;
            case R.id.navigation_notifications:
                transaction.replace(R.id.frame_layout_main, new NotificationFragment()).commit();
                return true;
            case R.id.navigation_settings:
                transaction.replace(R.id.frame_layout_main, new SettingsFragment()).commit();
                return true;
            case R.id.navigation_questions:
                transaction.replace(R.id.frame_layout_main, new QuestionFragment()).commit();
                return true;
        }
        return false;
    }
}