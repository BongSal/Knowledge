package com.knowledge.rupp.kosal.knowledge.model.databases;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginSessionManager  {
    // Shared Preference
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // Shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "Knowledge";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String PROFILE = "profile";
    public static final String PHONE = "phone";
    public static final String BACKGROUND = "background";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";

    public LoginSessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setUserInformation(String id, String name, String username, String email, String profile, String phone, String background) {
        editor.putString(ID, id);
        editor.putString(NAME, name);
        editor.putString(USERNAME, username);
        editor.putString(EMAIL, email);
        editor.putString(PROFILE, profile);
        editor.putString(PHONE, phone);
        editor.putString(BACKGROUND, background);
        editor.commit();
    }

    public String getInformation(String identifier) {
        return pref.getString(identifier, null);
    }

    public String getUserId() {
        return pref.getString(ID, null);
    }

    public void setInformation(String identifier, String value) {
        editor.putString(identifier, value);
        editor.commit();
    }

    public void setInformation(String identifier, int value) {
        editor.putInt(identifier, value);
        editor.commit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void logoutUser() {
        editor.clear().commit();
    }
}
