package com.knowledge.rupp.kosal.knowledge.networks;

import android.app.TaskStackBuilder;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.knowledge.rupp.kosal.knowledge.interfaces.NetworkResult;
import com.knowledge.rupp.kosal.knowledge.singletons.SingletonVolley;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Network {
    public static void getRequest(Context context, String url, final NetworkResult result) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        result.notifySuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        result.notifyError(error);
                    }
                }
        );
        SingletonVolley.getInstance(context).addToRequestQueue(request);
    }

    public static void postRequest(Context context, String url, final NetworkResult result, final Map<String, String> data) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        result.notifySuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        result.notifyError(error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return data;
            }
        };
        SingletonVolley.getInstance(context).addToRequestQueue(request);
    }
}
