package com.knowledge.rupp.kosal.knowledge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.dataset.AnswerQuestion;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.MyViewHolder>{

    private List<AnswerQuestion> dataList;
    private Context context;
    private final String QUESTIONS = "answers";
    private StorageReference reference = FirebaseStorage.getInstance().getReference(QUESTIONS);

    public AnswersAdapter(Context context, List<AnswerQuestion> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public AnswersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer_question, parent, false);
        return new AnswersAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AnswersAdapter.MyViewHolder holder, int position) {
        holder.name.setText(dataList.get(position).getName());
        holder.time.setText(dataList.get(position).getCreated_at());
        holder.content.setText(dataList.get(position).getContent());
        holder.like.setText(dataList.get(position).getLike());
        if (dataList.get(position).getImage() != null)
            Glide.with(context).using(new FirebaseImageLoader()).load(reference.child(dataList.get(position).getImage())).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.like_answer) TextView like;
        @BindView(R.id.time_answer) TextView time;
        @BindView(R.id.name_answer) TextView name;
        @BindView(R.id.content_answer) TextView content;
        @BindView(R.id.image_view) ImageView imageView;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}