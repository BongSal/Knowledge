package com.knowledge.rupp.kosal.knowledge.utils;

public class Constant {
    public static final String KEY = "key";
    public static final String TITLE = "title";
    public static final String IMAGE = "image";
    public static final String AUTHOR = "author";
    public static final String COMMENT = "comment";
    public static final String RATE = "rate";
    public static final String LIKE = "like";
    public static final String VIEW = "view";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String POSTED_BY = "posted_by";
    public static final String CONTENT = "content";
    public static final String ANSWERS_QUESTION = "answers_question";
    public static final String USER = "user";
}
