package com.knowledge.rupp.kosal.knowledge.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.knowledge.rupp.kosal.knowledge.R;
import com.knowledge.rupp.kosal.knowledge.model.dataset.Comments;
import com.knowledge.rupp.kosal.knowledge.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder>{

    public interface OnItemClickListener {
        void onLikeClickListener(int position);
        void onReplyClickListener(int position);
    }

    private List<Comments> dataList;
    private final String TAG = CommentsAdapter.class.getSimpleName();
    private Context context;
    private FirebaseStorage storage =  FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference().child("users");
    private OnItemClickListener listener;


    public CommentsAdapter(Context context, List<Comments> dataList, OnItemClickListener listener) {
        this.dataList = dataList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public CommentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new CommentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.MyViewHolder holder, int position) {
        holder.nameComment.setText(dataList.get(position).getName());
        holder.contentComment.setText(dataList.get(position).getContent());
        Glide.with(context).using(new FirebaseImageLoader()).load(storageReference.child(dataList.get(position).getImage())).into(holder.imageComment);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_comment) TextView nameComment;
        @BindView(R.id.content_comment) TextView contentComment;
        @BindView(R.id.image_comment) ImageView imageComment;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
