package com.knowledge.rupp.kosal.knowledge.model.dataset;

public class Comments {
    private String key, content, created_at, image, name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Comments(String key, String content, String created_at, String image, String name) {
        this.key = key;
        this.content = content;
        this.created_at = created_at;
        this.image = image;
        this.name = name;
    }
}
